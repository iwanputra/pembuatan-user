var express = require('express');
var router = express.Router();
var agentsController = require('../controllers/agents.js');

/* GET users listing. */
router.get('/', function(req, res, next) {
    agentsController.getAll(req, res);
});

router.get('/:id', function(req, res, next) {
    agentsController.getAgent(req, res);
});

router.post('/:id?', function(req, res, next) {
    agentsController.create(req, res);
});

router.put('/:id', function(req, res, next) {
    agentsController.update(req, res);
});

router.delete('/:id', function(req, res, next) {
    agentsController.deleteAgent(req, res);
});

module.exports = router;
