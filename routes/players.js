var express = require('express');
var router = express.Router();
var playersController = require('../controllers/players.js');

router.get('/', function(req, res, next) {
  playersController.getAll(req, res);
});

router.get('/:id', function(req, res, next) {
  playersController.getPlayer(req, res);
});

router.post('/players', function(req, res, next) {
  playersController.createPlayer(req, res);
});

router.put('/:id', function(req, res, next) {
  playersController.updatePlayer(req, res);
});

router.delete('/:id', function(req, res, next) {
  playersController.deletePlayer(req, res);
});

module.exports = router;
