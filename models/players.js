'use strict';

const { Model } = require('sequelize');

/*
module.exports = (sequelize, DataTypes) => {
  class Players extends Model {
    static associate(models) {
      this.belongsTo(models.Agent, {
        foreignKey: 'agent_id'
      });
    }
  };

  Players.init({
    agent_id: {
      type: DataTypes.INTEGER
    },
    userId: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    displayName: DataTypes.STRING,
    badge: {
      type: DataTypes.ENUM,
      values: ['gold', 'standart']
    },
    token: DataTypes.STRING(255),
    created_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW()
    },
    updated_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW(),
      onUpdate: DataTypes.NOW()
    }
  }, {
    sequelize,
    modelName: 'players',
  });

  return Players;
};
*/

module.exports = (sequelize, DataTypes) => {
  const Players = sequelize.define('players', {
    agent_id: {
      type: DataTypes.INTEGER
    },
    userId: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    displayName: DataTypes.STRING,
    badge: {
      type: DataTypes.ENUM,
      values: ['gold', 'standart']
    },
    token: DataTypes.STRING(255),
    created_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW()
    },
    updated_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW(),
      onUpdate: DataTypes.NOW()
    }
  });

  Players.associate = function(models) {
    // this.belongsTo(models.Agent, {
    //   foreignKey: 'agent_id'
    // });
  }

  return Players;
}