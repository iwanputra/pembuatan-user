'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Agent extends Model {
    static associate(models) {
    //   hasMany(models.Players, {
    //     foreignKey: 'agent_id'
    //   });
    }
  };

  Agent.init({
    parent_id: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    username: {
      type: DataTypes.STRING(35), 
      allowNull: false,
      unique: true
    },
    agent_code: {
      type: DataTypes.STRING(10),
      allowNull: false,
      unique: true
    },
    password: { type: DataTypes.STRING(255), allowNull: false },
    api_key: { type: DataTypes.STRING(255), allowNull: false },
    email: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: true
    },
    phone: {
      type: DataTypes.STRING(12),
      unique: true
    },
    norek: { type: DataTypes.STRING(50) },
    bank_name: {
      type: DataTypes.STRING(35)
    },
    role: {
      type: DataTypes.ENUM,
      values: ['master', 'agent'],
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW()
    },
    updated_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW(),
      onUpdate: DataTypes.NOW()
    }
  }, {
    sequelize,
    modelName: 'agent',
  });

  return Agent;
};

/*
module.exports = (sequelize, DataTypes) => {
    const Agent = sequelize.define('agent', {
        parent_id: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        username: {
            type: DataTypes.STRING(35), 
            allowNull: false,
            unique: true
        },
        agent_code: {
            type: DataTypes.STRING(10),
            allowNull: false,
            unique: true
        },
        password: { type: DataTypes.STRING(255), allowNull: false },
        api_key: { type: DataTypes.STRING(255), allowNull: false },
        email: {
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: true
        },
        phone: {
            type: DataTypes.STRING(12),
            unique: true
        },
        norek: { type: DataTypes.STRING(50) },
        bank_name: { type: DataTypes.STRING(35) },
        role: {
            type: DataTypes.ENUM,
            values: ['master', 'agent'],
            allowNull: true
        },
        created_at: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW()
        },
        updated_at: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW(),
            onUpdate: DataTypes.NOW()
        }
    });

    Agent.associate = function(models) {
        // Agent.hasMany(models.Player, {
        //     foreignKey: 'agent_id'
        // });
    }

    return Agent;
}
*/