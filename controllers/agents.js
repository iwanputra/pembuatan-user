var Agent = require('../models/agent.js');

const getAll = async (req, res) => {
    const agents = await Agent.findAll();
    if (players) {
        res.json({ 'data': agents });
    } else {
        res.json({ 'message': 'Data is empty' });
    }
}

const getAgent = async (req, res) => {
    const agent = await Agent.findAll({
        where: { id: req.params.id }
    })
    
    if (agent) {
        res.json({ 'data': agent[0] });
    } else {
        res.json({ 'message': 'Agent not found' });
    }
}

const createAgent = async (req, res) => {
    console.log(req.body);
    await Agent.create({
        parent_id: req.params.id ? req.params.id : 0,
        username: req.body.username,
        agent_code: req.body.agent_code,
        password: req.body.password,
        api_key: req.body.api_key,
        email: req.body.email,
        phone: req.body.phone,
        norek: req.body.norek,
        bank_name: req.body.bank_name,
        role: req.body.role
    }).then((agent) => {
        var status = agent ? 'OK' : 'Failed';
        res.json({ 'message': 'Create agent ' + status });
    });
}

const updateAgent = async (req, res) => {
    await Agent.update(req.body, {
        where: { id: req.params.id }
    }).then((agent) => {
        var status = agent ? 'OK' : 'Failed';
        res.json({ 'message': 'Update ' + status });
    });
}

const deleteAgent = async (req, res) => {
    await Agent.destroy({
        where: { id: req.params.id }
    }).then((agent) => {
        var status = agent ? 'OK' : 'Failed';
        res.json({ 'message': 'Delete ' + status });
    });
}

module.exports = {
    getAll,
    getAgent,
    createAgent,
    updateAgent,
    deleteAgent
}