const { v4: uuidv4 } = require('uuid');
const { Players } = require('../models/players.js');

const getAll = async (req, res) => {
    const players = await Players.findAll();
    if (players) {
        res.json({ 'data': players });
    } else {
        res.json({ 'message': 'Data is empty' });
    }
}

const getPlayer = async (req, res) => {
    const player = await Players.findAll({
        where: { id: req.params.id }
    })
    
    if (player) {
        res.json({ 'data': player[0] });
    } else {
        res.json({ 'message': 'Player not found' });
    }
}

const createPlayer = async (req, res) => {
    console.log(req.body);
    await Players.create({
        agent_id: req.params.id,
        userId: uuidv4(),
        displayName: req.body.username,
        badge: req.body.badge,
        token: req.body.token
    }).then((player) => {
        var status = player ? 'OK' : 'Failed';
        res.json({ 'message': 'Create ' + status })
    });
}

const updatePlayer = async (req, res) => {
    await Players.update(req.body, {
        where: { id: req.params.id }
    }).then((player) => {
        var status = player ? 'OK' : 'Failed';
        res.json({ 'message': 'Update ' + status });
    });
}

const deletePlayer = async (req, res) => {
    await Players.destroy({
        where: { id: req.params.id }
    }).then((player) => {
        var status = player ? 'OK' : 'Failed';
        res.json({ 'message': 'Delete ' + status });
    });
}

module.exports = {
    getAll,
    getPlayer,
    createPlayer,
    updatePlayer,
    deletePlayer
}