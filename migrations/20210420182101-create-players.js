'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('players', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      agent_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'agents',
          key: 'id'
        }
      },
      userId: {
        type: Sequelize.STRING(50),
        unique: true
      },
      displayName: {
        type: Sequelize.STRING(50)
      },
      badge: {
        type: Sequelize.ENUM,
        values: [ 'gold', 'standart' ]
      },
      token: {
        type: Sequelize.STRING(255)
      },
      createdAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW()
      },
      updatedAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW(),
        onUpdate: Sequelize.NOW()
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('players');
  }
};