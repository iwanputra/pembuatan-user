'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('agents', {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
      },
      parent_id: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      username: {
        type: Sequelize.STRING(255),
        unique: true
      },
      agent_code: {
        type: Sequelize.STRING(10),
        unique: true
      },
      password: {
        type: Sequelize.STRING(255)
      },
      api_key: {
        type: Sequelize.STRING(255),
        unique: true
      },
      email: {
        type: Sequelize.STRING(50),
        unique: true
      },
      phone: {
        type: Sequelize.STRING(12),
        unique: true
      },
      norek: {
        type: Sequelize.STRING(30)
      },
      bank_name: {
        type: Sequelize.STRING(35)
      },
      role: {
        type: Sequelize.ENUM,
        values: [ 'master', 'agent' ]
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW()
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
        onUpdate: Sequelize.NOW()
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('agents');
  }
};